import formJson0 from './formJson0.json';
import formJson1 from './formJson1.json';
import formJson2 from './formJson2.json';
import formJson3 from './formJson3.json';
import formJson4 from './formJson4.json';
import formJson5 from './formJson5.json';
import formJson6 from './formJson6.json';
import formJson7 from './formJson7.json';
import formJson8 from './formJson8.json';
import formJson9 from './formJson9.json';
import formJson10 from './formJson10.json';
import formJsonAll from './formJsonAll.json';
import formJsonScoring from './formJsonScoring.json';

let jsons = [
    formJson0,
    formJson1,
    formJson2,
    formJson3,
    formJson4,
    formJson5,
    formJson6,
    formJson7,
    formJson8,
    formJson9,
    formJson10,
    formJsonAll,
    formJsonScoring
];

let preAreas = [
    'text0',
    'text1',
    'text2',
    'text3',
    'text4',
    'text5',
    'text6',
    'text7',
    'text8',
    'text9',
    'text10',
    'textAll',
    'textScoring'
]

let copyAreas = [
    'copy0',
    'copy1',
    'copy2',
    'copy3',
    'copy4',
    'copy5',
    'copy6',
    'copy7',
    'copy8',
    'copy9',
    'copy10',
    'copyAll',
    'copyScoring'
]

document.getElementById("convert").addEventListener('click', () => {

    let i = 0;
    for (let i = 0; i < jsons.length; i++) {
        let preArea = document.getElementById(preAreas[i]);
        try {
            test(jsons[i]);
            preArea.innerText = JSON.stringify(jsons[i], null, 1);
        } catch (error) {
            console.log(error);
        }
    }



});


function test(item) {
    Object.keys(item).forEach((key) => {
        if (key === 'inputDescriptor' && item[key]['fieldDisplay'] &&
            (
                item[key]['fieldDisplay'] !== 'INPUT_TEXT' &&
                item[key]['fieldDisplay'] !== 'SELECT' &&
                item[key]['fieldDisplay'] !== 'RADIO_SELECT' &&
                item[key]['fieldDisplay'] !== 'DATE' &&
                item[key]['fieldDisplay'] !== 'CHECKBOX' &&
                item[key]['fieldDisplay'] !== 'LABEL' &&
                item[key]['fieldDisplay'] !== 'SIGNATURE' &&
                item[key]['fieldDisplay'] !== 'ENTITY_SEARCH'
            )
        ) {
            const display = item[key]['fieldDisplay'];
            item[key]['fieldDisplay'] = 'CUSTOM';
            item[key]['customDisplayAttribute'] = display;
        }
        else if (typeof item[key] == 'object' && key !== 'additionalProperties') {
            test(item[key])
        }
    })

}

function copyToClipboard(elementId) {
    const jsonTextEl = document.getElementById(elementId);
    const tempTextarea = document.createElement('textarea');

    tempTextarea.id = 'tempTextarea';
    tempTextarea.style.height = '0px';
    document.body.appendChild(tempTextarea);
    tempTextarea.value = jsonTextEl.innerText;

    jsonTextEl.focus();
    tempTextarea.select();
    document.execCommand('copy');

    document.body.removeChild(tempTextarea);
}

for (let i = 0; i < jsons.length; i++) {
    let copyArea = document.getElementById(copyAreas[i]);
    copyArea.addEventListener('click', function () {
        copyToClipboard(preAreas[i]);
    });
}